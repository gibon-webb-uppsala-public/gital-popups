��    ,      |      �      �     �     �     �  $        1     ?  
   V     a     |     �     �  $   �  *   �  
   �  
   �  )   �  +   '     S  	   Y     c     }  I   �  Q   �  K   *  �   v  Q        Z     i     u     �     �     �     �  �   �     k     y  +   �  Z   �  
   	       6         W     ]  �  }     
     -
     A
      G
     h
     {
     �
     �
     �
  
   �
     �
  +   �
  *   �
     %  	   4     >  '   Z     �     �  #   �     �  	   �     �     �     �               /     ;     B     H     O     a  
   z     �     �  $   �  
   �  
   �     �  /   �             Add New Add New Popup Add New on ToolbarPopup Add an additional class to the popup Add condition Admin Menu textPopups All Popups Continue to show the popup Custom Delay Disable after Don't show the popup after this time Don't show the popup if this cookie is set Edit Popup Everywhere It should not be shown anymore for 7 days It should not be shown anymore this session Never New Popup No Popups found in Trash. No Popups found. Overrides the “Featured Image” phrase for this post type.Popup Image Overrides the “Remove featured image” phrase for this post type.Remove image Overrides the “Set featured image” phrase for this post type.Set image Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).Uploaded to this Popup Overrides the “Use as featured image” phrase for this post type.Use as image Parent Popups: Popup title Post type general namePopups Post type singular namePopup Posts Product categories Render the popup with REST Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”.Popups list Search Popups Settings Show the popup after this amount of seconds The post type archive label used in nav menus. Default “Post Archives”.Popup archives View Popup View Popups What should happen when the popup has been showen once Where Where should the popup be shown Project-Id-Version: Gital Popups
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-11-04 13:41+0000
PO-Revision-Date: 2023-09-18 12:56+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.3; wp-6.1
X-Domain: gital-popups Lägg till ny Lägg till ny popup Popup Lägg till en klass till popupen Lätt till villkor Popups Alla popups Fortsätt att visa popupen Anpassad Visa efter Inaktivera efter Visa inte popupen efter den här tidpunkten Visa inte popupen om denna cookie är satt Redigera popup Överallt Den skall döljas i 7 dagar Den skall inte visas mer för sessionen Aldrig Ny popup Inga popuper funna i papperskorgen. Inga popuper funna. Popupbild Ta bort bild Ställ in bild Uppladdade till denna popup Använd som bild Föräldrapopups: Popup titel Popups Popup Poster Produktkategorier Rendera popupen med REST Popuplista Sök popups Inställningar Visa popupen efter såhär lång tid Popuparkiv Visa popup Visa popups Vad ska hända när popupen har visats en gång Vart Vart ska popupen visas 