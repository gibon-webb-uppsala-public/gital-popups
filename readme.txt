=== Gital Popups ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 5.6
Requires PHP: 7.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Popups is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.

== Description ==
The Gital Popups is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.

== Changelog ==

= 2.3.0 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 2.2.4 - 2023.10.02 = 
* Bugfix: Aborted the no rest handler if there is no popups to show.

= 2.2.3 - 2023.09.14 = 
* Update: The popups is now handling forms through Ninja Forms and other element that is not supposed to render with REST.

= 2.1.0 - 2023.09.14 = 
* Update: Popup is now registred as seen when popup is shown.
* Update: Popup can now be discarted by the name of a cookie.

= 2.0.3 - 2023.06.29 = 
* Update: Each popup can now have a custom class.
* Update: The post type is now named g_popups instead of popups.
* Update: Better handling of the columns data.

= 1.2.1 - 2023.05.15 = 
* Bugfix: If WC was activated, the custom selection of where to show the popup broke.

= 1.2.0 - 2023.03.29 = 
* Update: Gibon Webb Uppsala is now Gibon Webb.

= 1.1.2 - 2022.12.14 = 
* Update: Bumped the script version.

= 1.1.0 - 2022.12.14 = 
* Update: Added columns to the admin.
* Bugfix: The popups did not render correctly if there where no "disable_after" value set.
* Bugfix: The fields where not correctly set up is WC where not installed.

= 1.0.0 - 2022.11.03 = 
* Update: Init.