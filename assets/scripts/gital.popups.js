/**
 * Fetch the popups
 *
 * @param {Array} payload
 *
 * @return {*} Returns true if there are any popups on this page
 */
const fetch_popups = async ( payload ) => {
	const response = await fetch( '/wp-json/gital/popups/fetch_popups', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify( {
			id: payload.id,
			type: payload.type,
		} ),
	} );
	return response.json();
};

/**
 * Handling the shown popups
 *
 * @param {Object} popup The popup element
 */
const shown_popup_handler = ( popup ) => {
	let cookie_payload;
	const cookie_data = {
		id: popup.parameters.popup_id,
		time_shown: Date.now(),
	};
	let stored_cookie;
	let previously_showed_popups = [];
	switch ( popup.parameters.after_shown ) {
		case 'week':
			stored_cookie = g_get_cookie( 'g-popups-week' );
			if ( stored_cookie ) {
				previously_showed_popups = JSON.parse( stored_cookie );
				previously_showed_popups.forEach(
					( previously_showed_popup, index ) => {
						if (
							popup.parameters.popup_id ===
							previously_showed_popup.id
						) {
							previously_showed_popups.splice( index, 1 );
						}
					},
				);
			}
			cookie_payload = JSON.stringify( [
				...previously_showed_popups,
				cookie_data,
			] );
			g_set_cookie( 'g-popups-week', cookie_payload, 7, 'Strict' );
			break;
		case 'session':
			stored_cookie = g_get_cookie( 'g-popups-session' );
			if ( stored_cookie ) {
				previously_showed_popups = JSON.parse( stored_cookie );
				previously_showed_popups.forEach(
					( previously_showed_popup, index ) => {
						if (
							popup.parameters.popup_id ===
							previously_showed_popup.id
						) {
							previously_showed_popups.splice( index, 1 );
						}
					},
				);
			}
			cookie_payload = JSON.stringify( [
				...previously_showed_popups,
				cookie_data,
			] );
			g_set_cookie( 'g-popups-session', cookie_payload, 0, 'Strict' );
			break;
	}
};

/**
 * Control witch popups that should be shown
 *
 * @param {Array} popups An array of popup objects
 * @return {Array} An array of popups that should be shown
 */
const control_popups = ( popups ) => {
	let popups_to_render = [];

	popups.forEach( ( popup ) => {
		let stored_cookie;
		let previously_showed_popups;
		let discart_popup = false;

		// Control if there is a defined cookie that should discart the popup
		if ( popup.parameters.disable_cookie ) {
			const cookie_control = g_get_cookie(
				popup.parameters.disable_cookie,
			);
			if ( cookie_control ) {
				discart_popup = true;
			}
		}

		// Control if the popup has been shown and should not be shown anymore
		switch ( popup.parameters.after_shown ) {
			case 'week':
				stored_cookie = g_get_cookie( 'g-popups-week' );
				if ( stored_cookie ) {
					previously_showed_popups = JSON.parse( stored_cookie );
					previously_showed_popups.forEach(
						( previously_showed_popup ) => {
							if (
								popup.parameters.popup_id ===
								previously_showed_popup.id
							) {
								const a_week_ago = new Date( new Date().getTime() - ( 7 * 24 * 60 * 60 * 1000 ) ).valueOf();

								if (
									a_week_ago <
									previously_showed_popup.time_shown
								) {
									discart_popup = true;
								}
							}
						},
					);
				}
				break;
			case 'session':
				stored_cookie = g_get_cookie( 'g-popups-session' );
				if ( stored_cookie ) {
					previously_showed_popups = JSON.parse( stored_cookie );
					previously_showed_popups.forEach(
						( previously_showed_popup ) => {
							if (
								popup.parameters.popup_id ===
								previously_showed_popup.id
							) {
								discart_popup = true;
							}
						},
					);
				}
				break;
		}

		// Control if the popups due date has passed
		if (
			popup.parameters.disable_after &&
			Math.floor( Date.now() / 1000 ) > popup.parameters.disable_after
		) {
			discart_popup = true;
		}

		// Append the popup if it should be shown
		if ( ! discart_popup ) {
			popups_to_render = [ ...popups_to_render, popup ];
		}
	} );

	return popups_to_render;
};

/**
 * Render the popups
 *
 * @param {Array}  popups  An array of the popups objects
 * @param {number} counter The amount of milliseconds that the call has taken
 */
const render_popups = ( popups, counter ) => {
	const body = document.querySelector( 'body' );
	const popups_anchor = document.createElement( 'div' );
	popups_anchor.setAttribute( 'id', 'g-popups-anchor' );
	popups_anchor.setAttribute( 'class', 'g-popups' );

	const popups_to_render = control_popups( popups );

	if ( ! popups_to_render ) {
		return;
	}

	popups_to_render.forEach( ( popup ) => {
		if ( ! popup.parameters.no_rest ) {
			const popup_modal = document.createElement( 'div' );
			popup_modal.setAttribute( 'class', 'g-popups__item' );
			popup_modal.innerHTML = popup.modal;
			popups_anchor.appendChild( popup_modal );
		}

		let delay = ( popup.parameters.delay * 1000 ) - counter;

		if ( 0 > delay ) {
			delay = 0;
		}

		setTimeout( () => {
			window.g_show_modal_from_id( popup.parameters.popup_modal_id );
			shown_popup_handler( popup );
		}, delay );
	} );
	body.appendChild( popups_anchor );
};

/**
 * Popups Initiator
 *
 * @param {Object} payload The id and type of the current page
 */
const popups_init = ( payload ) => {
	let counter = 0;
	const counter_interval = setInterval( () => {
		counter = counter + 100;
	}, 100 );
	fetch_popups( payload ).then( ( response ) => {
		if ( response ) {
			render_popups( response, counter );
			clearInterval( counter_interval );
		}
	} );
};
window.g_popups = {
	init: popups_init,
};
