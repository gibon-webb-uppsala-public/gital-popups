<?php
/**
 * Popup handler
 *
 * @package Gital Blocks
 */

namespace gital_popups;

use \gital_library\Modal;
use function \gital_library\clean_classes;

if ( ! class_exists( 'Popup_Handler' ) ) {
	/**
	 * Popup handler
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.1.2
	 */
	class Popup_Handler {
		public function __construct() {
			add_action( 'rest_api_init', array( $this, 'popups_rest_routes' ) );
			add_action( 'wp_footer', array( $this, 'no_rest_handler' ) );
		}

		/**
		 * No REST handler
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 */
		public function no_rest_handler() {
			$parameters = Init::get_page_parameters();

			// Get array of popups to show.
			$popups_to_show = self::get_popups_to_show( $parameters['id'], $parameters['type'] );

			if ( ! $popups_to_show ) {
				return;
			}

			// Get array of popup arrays.
			$popup_objects = self::get_popup_objects( $popups_to_show );

			// Render the popups that should not render through REST.
			foreach ( $popup_objects as $popup_object ) {
				if ( $popup_object['parameters']['no_rest'] ) {
					echo $popup_object['modal']; //phpcs:ignore
				}
			}
		}

		/**
		 * Popups REST routes
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function popups_rest_routes() {
			register_rest_route(
				'gital',
				'/popups/fetch_popups',
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'fetch_popups_callback' ),
					'permission_callback' => '__return_true',
				)
			);
		}

		/**
		 * Fetch popups callback
		 *
		 * @return array The popup objects.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function fetch_popups_callback( $payload ) {
			$parameters = $payload->get_params();

			$id   = $parameters['id'] ?? false;
			$type = $parameters['type'] ?? false;

			// Get array of popups to show.
			$popups_to_show = self::get_popups_to_show( $id, $type );

			// Get array of popup arrays.
			$popup_objects = self::get_popup_objects( $popups_to_show );

			return $popup_objects;
		}

		/**
		 * Get popup objects
		 *
		 * @param array $popups_to_show An array of ids to popups.
		 *
		 * @return array An array of arrays with popups ready to render.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 */
		private function get_popup_objects( $popups_to_show ) {
			if ( empty( $popups_to_show ) ) {
				return false;
			}

			$popup_objects = array();

			foreach ( $popups_to_show as $popup_to_show ) {
				$popup_post     = get_post( $popup_to_show );
				$popup_modal_id = 'g-popup--' . $popup_to_show;
				$popup_content  = apply_filters( 'the_content', $popup_post->post_content );

				$popup = new Modal( $popup_content, $popup_modal_id, clean_classes( array( 'g-popup', get_field( 'additional_class', $popup_to_show ) ) ), get_the_title( $popup_to_show ), true, true, false );

				$popup_objects[] = array(
					'parameters' => array(
						'popup_id'       => $popup_to_show,
						'popup_modal_id' => $popup_modal_id,
						'after_shown'    => get_field( 'after_shown', $popup_to_show ),
						'disable_cookie' => get_field( 'disable_cookie', $popup_to_show ),
						'delay'          => get_field( 'delay', $popup_to_show ),
						'disable_after'  => strtotime( get_field( 'disable_after', $popup_to_show ) ),
						'no_rest'        => has_block( 'acf/gital-form', $popup_to_show ) || ! get_field( 'rest', $popup_to_show ),
					),
					'modal'      => $popup->get_modal(),
				);
			}

			return $popup_objects;
		}

		/**
		 * Get popups that should be shown on this page
		 *
		 * @param int $id The id of the current page.
		 * @param string $type The the type of the page.
		 *
		 * @return array The ids of the popups that should be shown.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		private function get_popups_to_show( $id, $type ) {

			// Get all the popups.
			$the_popups = get_posts(
				array(
					'fields'         => 'ids',
					'post_type'      => 'g_popup',
					'post_status'    => 'publish',
					'posts_per_page' => -1,
				)
			);

			// Return false if there are no popups.
			if ( empty( $the_popups ) ) {
				return false;
			}

			// Create a blank array of popups that should be shown.
			$popups_to_show = array();

			// Iterate through the popups and fetch the popups that should be shown on this page.
			foreach ( $the_popups as $the_popup ) {
				// Should the popup be shown everywhere.
				$where = get_field( 'where', $the_popup );

				if ( 'everywhere' === $where ) {
					$popups_to_show[] = $the_popup;
					continue;
				} else {

					// Return if there are no type or id.
					if ( empty( $type ) || empty( $id ) ) {
						continue;
					}

					$custom = get_field( 'custom', $the_popup );

					foreach ( $custom as $custom_property ) {

						// Should the popup be shown on this page.
						if ( 'single' === $type && 'posts' === $custom_property['acf_fc_layout'] ) {
							foreach ( $custom_property['post'] as $post ) {
								if ( $id === $post ) {
									$popups_to_show[] = $the_popup;
									continue 3;
								}
							}
						}

						// Should the popup be shown on this product category.
						if ( ( 'prod_category' === $type || 'product' === get_post_type( $id ) ) && 'product_categories' === $custom_property['acf_fc_layout'] ) {
							foreach ( $custom_property['categories'] as $category ) {
								// If the current product category is the selecteed category.
								if ( 'prod_category' === $type && $id === $category ) {
									$popups_to_show[] = $the_popup;
									continue 3;
								}

								// If the current product belongs to the category.
								if ( 'prod_category' === $type ) {
									$this_categorys_ancestors = get_ancestors( $id, 'product_cat' );

									if ( ! empty( $this_categorys_ancestors ) && in_array( $category, $this_categorys_ancestors, false ) ) {
										$popups_to_show[] = $the_popup;
										continue 3;
									}
								}

								// If the current product belongs to the category.
								if ( 'single' === $type && has_term( $category, 'product_cat', $id ) ) {
									$popups_to_show[] = $the_popup;
									continue 3;
								}
							}
						}
					}
				}
			}

			// Return false if there are no popups to show.
			if ( empty( $popups_to_show ) ) {
				return false;
			}

			return $popups_to_show;
		}
	}
}
