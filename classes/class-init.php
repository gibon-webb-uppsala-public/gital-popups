<?php
/**
 * Init
 *
 * @package Gital Popups
 */

namespace gital_popups;

if ( ! class_exists( 'Init' ) ) {
	/**
	 * Init
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.1.0
	 */
	class Init {
		public function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'init_script' ) );
		}

		/**
		 * Init script
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init_script() {
			wp_add_inline_script( 'g_popups_script', 'document.addEventListener("load", g_popups.init(' . wp_json_encode( self::get_page_parameters() ) . '))', 'after' );
		}

		/**
		 * Get the page parameters
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 */
		public static function get_page_parameters() {
			$parameters = array(
				'type' => false,
				'id'   => false,
			);

			if ( is_singular() ) {
				$parameters = array(
					'type' => 'single',
					'id'   => get_the_ID(),
				);
			}
			if ( \gital_library\is_wc_activated() && is_product_category() ) {
				$product_category_object = get_queried_object();
				$product_category_id     = $product_category_object->term_id;
				$parameters              = array(
					'type' => 'prod_category',
					'id'   => $product_category_id,
				);
			}

			return $parameters;
		}
	}
}
