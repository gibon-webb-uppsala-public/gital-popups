<?php
/**
 * Post Type
 *
 * @package Gital Popups
 */

namespace gital_popups;

if ( ! class_exists( 'Post_Type' ) ) {
	/**
	 * Post Type
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.2.0
	 */
	class Post_Type {
		/**
		 * Construct
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function __construct() {
			add_action( 'init', array( $this, 'create_popup_post_type' ) );
			add_filter( 'enter_title_here', array( $this, 'popup_title_placeholder' ), 20, 2 );
			add_filter( 'acf/init', array( $this, 'register_popup_fields' ) );
			add_filter( 'manage_g_popup_posts_columns', array( $this, 'manage_popup_columns' ) );
			add_action( 'manage_g_popup_posts_custom_column', array( $this, 'manage_popup_columns_content' ), 10, 2 );
		}

		/**
		 * Registers the Popups posttype
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public function create_popup_post_type() {
			register_post_type(
				'g_popup',
				array(
					'labels'        => array(
						'name'                  => _x( 'Popups', 'Post type general name', 'gital-popups' ),
						'singular_name'         => _x( 'Popup', 'Post type singular name', 'gital-popups' ),
						'menu_name'             => _x( 'Popups', 'Admin Menu text', 'gital-popups' ),
						'name_admin_bar'        => _x( 'Popup', 'Add New on Toolbar', 'gital-popups' ),
						'add_new'               => __( 'Add New', 'gital-popups' ),
						'add_new_item'          => __( 'Add New Popup', 'gital-popups' ),
						'new_item'              => __( 'New Popup', 'gital-popups' ),
						'edit_item'             => __( 'Edit Popup', 'gital-popups' ),
						'view_item'             => __( 'View Popup', 'gital-popups' ),
						'view_items'            => __( 'View Popups', 'gital-popups' ),
						'all_items'             => __( 'All Popups', 'gital-popups' ),
						'search_items'          => __( 'Search Popups', 'gital-popups' ),
						'parent_item_colon'     => __( 'Parent Popups:', 'gital-popups' ),
						'not_found'             => __( 'No Popups found.', 'gital-popups' ),
						'not_found_in_trash'    => __( 'No Popups found in Trash.', 'gital-popups' ),
						'featured_image'        => _x( 'Popup Image', 'Overrides the “Featured Image” phrase for this post type.', 'gital-popups' ),
						'set_featured_image'    => _x( 'Set image', 'Overrides the “Set featured image” phrase for this post type.', 'gital-popups' ),
						'remove_featured_image' => _x( 'Remove image', 'Overrides the “Remove featured image” phrase for this post type.', 'gital-popups' ),
						'use_featured_image'    => _x( 'Use as image', 'Overrides the “Use as featured image” phrase for this post type.', 'gital-popups' ),
						'archives'              => _x( 'Popup archives', 'The post type archive label used in nav menus. Default “Post Archives”.', 'gital-popups' ),
						'insert_into_item'      => _x( 'Insert into Popup', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post).', 'gital-popups' ),
						'uploaded_to_this_item' => _x( 'Uploaded to this Popup', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).', 'gital-popups' ),
						'filter_items_list'     => _x( 'Filter Popups list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”.', 'gital-popups' ),
						'items_list_navigation' => _x( 'Popups list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”.', 'gital-popups' ),
						'items_list'            => _x( 'Popups list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”.', 'gital-popups' ),
					),

					'description'   => __( 'Holds our popups specific data', 'gital-popups' ),
					'public'        => false,
					'show_ui'       => true,
					'show_in_menu'  => true,
					'has_archive'   => false,
					'menu_icon'     => 'dashicons-slides',
					'menu_position' => 10,
					'show_in_rest'  => true,
					'supports'      => array( 'title', 'editor' ),
					'hierarchical'  => false,
					'taxonomies'    => array( '' ),
				)
			);
		}

		/**
		 * Set the title placeholder for popups
		 *
		 * @param string $title
		 *
		 * @return string
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function popup_title_placeholder( $title, $post ) {
			if ( 'g_popup' === $post->post_type ) {
				$title = __( 'Popup title', 'gital-popups' );
				return $title;
			}

			return $title;
		}

		/**
		 * Register the fields for the popups
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.1
		 */
		public function register_popup_fields() {
			$fields_arguments = array(
				'key'                   => 'group_settings',
				'title'                 => __( 'Settings', 'gital-popups' ),
				'fields'                => array(
					array(
						'key'           => 'field_where',
						'label'         => __( 'Where should the popup be shown', 'gital-popups' ),
						'name'          => 'where',
						'type'          => 'button_group',
						'choices'       => array(
							'everywhere' => __( 'Everywhere', 'gital-popups' ),
							'custom'     => __( 'Custom', 'gital-popups' ),
						),
						'default_value' => 'everywhere',
						'return_format' => 'value',
						'layout'        => 'horizontal',
					),
					array(
						'key'               => 'field_custom',
						'label'             => __( 'Custom', 'gital-popups' ),
						'name'              => 'custom',
						'type'              => 'flexible_content',
						'conditional_logic' => array(
							array(
								array(
									'field'    => 'field_where',
									'operator' => '==',
									'value'    => 'custom',
								),
							),
						),
						'layouts'           => array(
							'layout_posts' => array(
								'key'        => 'layout_posts',
								'name'       => 'posts',
								'label'      => __( 'Posts', 'gital-popups' ),
								'display'    => 'block',
								'sub_fields' => array(
									array(
										'key'           => 'field_post',
										'name'          => 'post',
										'type'          => 'post_object',
										'return_format' => 'id',
										'multiple'      => 1,
										'allow_null'    => 0,
										'ui'            => 1,
									),
								),
							),
						),
						'button_label'      => __( 'Add condition', 'gital-popups' ),
					),
					array(
						'key'           => 'field_after_shown',
						'label'         => __( 'What should happen when the popup has been showen once', 'gital-popups' ),
						'name'          => 'after_shown',
						'type'          => 'select',
						'choices'       => array(
							'session' => __( 'It should not be shown anymore this session', 'gital-popups' ),
							'week'    => __( 'It should not be shown anymore for 7 days', 'gital-popups' ),
							'nothing' => __( 'Continue to show the popup', 'gital-popups' ),
						),
						'default_value' => 'session',
						'return_format' => 'value',
					),
					array(
						'key'   => 'field_disable_cookie',
						'label' => __( "Don't show the popup if this cookie is set", 'gital-popups' ),
						'name'  => 'disable_cookie',
						'type'  => 'text',
					),
					array(
						'key'           => 'field_rest',
						'label'         => __( 'Render the popup with REST', 'gital-popups' ),
						'name'          => 'rest',
						'type'          => 'true_false',
						'ui'            => 1,
						'default_value' => 1,
					),
					array(
						'key'            => 'field_disable_after',
						'label'          => __( 'Don\'t show the popup after this time', 'gital-popups' ),
						'name'           => 'disable_after',
						'type'           => 'date_time_picker',
						'display_format' => 'Y-m-d H:i:s',
						'return_format'  => 'Y-m-d H:i:s',
						'first_day'      => 1,
					),
					array(
						'key'           => 'field_delay',
						'label'         => __( 'Show the popup after this amount of seconds', 'gital-popups' ),
						'name'          => 'delay',
						'type'          => 'range',
						'default_value' => 5,
						'min'           => 1,
						'max'           => 120,
						'step'          => 1,
					),
					array(
						'key'   => 'field_additional_class',
						'label' => __( 'Add an additional class to the popup', 'gital-popups' ),
						'name'  => 'additional_class',
						'type'  => 'text',
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'g_popup',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'side',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => true,
				'description'           => '',
				'show_in_rest'          => 0,
			);
			if ( \gital_library\is_wc_activated() ) {
				$fields_arguments['fields'][1]['layouts']['layout_product_categories'] = array(
					'key'        => 'layout_product_categories',
					'name'       => 'product_categories',
					'label'      => __( 'Product categories', 'gital-popups' ),
					'display'    => 'block',
					'sub_fields' => array(
						array(
							'key'               => 'field_categories',
							'label'             => '',
							'name'              => 'categories',
							'type'              => 'taxonomy',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'taxonomy'          => 'product_cat',
							'add_term'          => 0,
							'save_terms'        => 0,
							'load_terms'        => 0,
							'return_format'     => 'id',
							'field_type'        => 'multi_select',
							'allow_null'        => 0,
							'multiple'          => 0,
						),
					),
					'min'        => '',
					'max'        => '',
				);
			}
			acf_add_local_field_group( $fields_arguments );
		}

		/**
		 * Manages the Popups admin columns
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function manage_popup_columns( $columns ) {
			$columns = array(
				'cb'            => $columns['cb'],
				'title'         => __( 'Title' ),
				'where'         => __( 'Where', 'gital-popups' ),
				'delay'         => __( 'Delay', 'gital-popups' ),
				'disable_after' => __( 'Disable after', 'gital-popups' ),
			);

			return $columns;
		}

		/**
		 * Manages the Popups admin columns content
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public function manage_popup_columns_content( $column, $post_id ) {
			// Where column.
			if ( 'where' === $column ) {
				if ( get_field( 'where' ) ) {
					echo esc_html( 'custom' === get_field( 'where' ) ? __( 'Custom', 'gital-popups' ) : __( 'Everywhere', 'gital-popups' ) );
				}
			}

			// Delay column.
			if ( 'delay' === $column ) {
				echo esc_html( ( get_field( 'delay' ) ?? '0' ) . 's' );
			}

			// Disable after column.
			if ( 'disable_after' === $column ) {
				echo esc_html( get_field( 'disable_after' ) ?: __( 'Never', 'gital-popups' ) );
			}
		}
	}
}
