<?php
/**
 * Plugin Name: Gital Popups
 * Author: Gibon Webb
 * Version: 2.3.0
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Popups is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se@gibon.se.
 *
 * @package Gital Popups
 */

namespace gital_popups;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Load textdomain and languages
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function textdomain() {
	load_plugin_textdomain( 'gital-popups', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_popups\textdomain' );

// Constants.
if ( ! defined( 'G_POPUPS_ROOT' ) ) {
	define( 'G_POPUPS_ROOT', plugins_url( '', __FILE__ ) );
}
if ( ! defined( 'G_POPUPS_ASSETS' ) ) {
	define( 'G_POPUPS_ASSETS', G_POPUPS_ROOT . '/assets' );
}
if ( ! defined( 'G_POPUPS_RESOURSES' ) ) {
	define( 'G_POPUPS_RESOURSES', G_POPUPS_ROOT . '/assets/resources' );
}
if ( ! defined( 'G_POPUPS_ROOT_PATH' ) ) {
	define( 'G_POPUPS_ROOT_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'G_POPUPS_ASSETS_PATH' ) ) {
	define( 'G_POPUPS_ASSETS_PATH', G_POPUPS_ROOT_PATH . 'assets/' );
}
if ( ! defined( 'G_POPUPS_FUNCTIONS_PATH' ) ) {
	define( 'G_POPUPS_FUNCTIONS_PATH', G_POPUPS_ROOT_PATH . 'functions/' );
}
if ( ! defined( 'G_POPUPS_CLASSES_PATH' ) ) {
	define( 'G_POPUPS_CLASSES_PATH', G_POPUPS_ROOT_PATH . 'classes/' );
}
if ( ! defined( 'G_POPUPS_VIEWS_PATH' ) ) {
	define( 'G_POPUPS_VIEWS_PATH', G_POPUPS_ROOT_PATH . 'views/' );
}

// Init the updater.
require G_POPUPS_ROOT_PATH . 'vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-popups.json',
	__FILE__,
	'gital-popups'
);

/**
 * Enquene public scripts
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function assets() {
	wp_register_style( 'g_popups_style', G_POPUPS_ASSETS . '/styles/gital.popups.min.css', array(), '1.0.0' );
	wp_enqueue_style( 'g_popups_style' );

	wp_register_script( 'g_popups_script', G_POPUPS_ASSETS . '/scripts/gital.popups.min.js', array(), '1.2.0', true );
	wp_enqueue_script( 'g_popups_script' );
}
add_action( 'wp_enqueue_scripts', 'gital_popups\assets' );

// Post types.
require_once G_POPUPS_CLASSES_PATH . 'class-post-type.php';
new Post_Type();

require_once G_POPUPS_CLASSES_PATH . 'class-init.php';
new Init();

require_once G_POPUPS_CLASSES_PATH . 'class-popup-handler.php';
new Popup_Handler();

